var layerTime;
$(window).on('load', function(){
    scrollBar();
    checkAll();
    gnbOpen();
    inputPassword();
});

$(function(){
    touchEvent();
    starPoint();
    amountEvent();
    calendarVertical();
    timeSelect();
    cardEdit();
    cartItemOption();
    containerScroll();
    heartToggle();
    inputInfoTooltip();
    editInline();
    tabEvent();
    sliderEvent();
    sortToggleEvent();
    quToggle();
    accordionEvent();
    reviewPicPop();
});

$(window).on('scroll', function(){
    var winST = $(window).scrollTop();
    if(winST > 0){
        $('#header').addClass('scrolled');
    } else {
        $('#header').removeClass('scrolled');
    }
});

function scrollBar(){
    $('.scrollbar-inner').scrollbar();
}

function checkAll(){
    if($('.accept').length > 0){
        $('.checkbox-all input').on('change', function(){
            if($(this).prop('checked') != false){
                $('.checkbox-wrap input').prop('checked', true);
            } else {
                $('.checkbox-wrap input').prop('checked', false);
            }
        });

        $('.checkbox-wrap input').on('change', function(){
            var chkLength = $('.checkbox-wrap input').length,
            chkedLength = $('.checkbox-wrap input:checked').length;
            if(chkLength != chkedLength){
                $('.checkbox-all input').prop('checked', false);
            } else {
                $('.checkbox-all input').prop('checked', true);
            }
        });
    }
}

function gnbOpen(){
    $('#header .btn-menu').on('click', function(){
        $('#gnb').addClass('on');
    });

    $('#gnb .overlay, #gnb .btn-logout').on('click', function(){
        $('#gnb').removeClass('on');
    });
}

function inputPassword(){
    $('.input-pw').each(function(){
        var inp = $(this);
        inp.find('.btn-show').on('click', function(){
            if(!inp.hasClass('on')){
                inp.addClass('on');
            } else {
                inp.removeClass('on');
            }
        });
        inp.find('input[type="password"]').on('keyup', function(){
            var inpVal = $(this).val();
            inp.find('input[type="text"]').val(inpVal);
        });
        inp.find('input[type="text"]').on('keyup', function(){
            var inpVal = $(this).val();
            inp.find('input[type="password"]').val(inpVal);
        });
    });
}

function touchEvent(){
    $('.hover').on({ 'touchstart' : function(){
            $(this).addClass('touch');
        }, 'touchend' : function(){
            $(this).removeClass('touch');
        }
    });

    $('.btn').on({ 'touchstart' : function(){
            $(this).addClass('touch');
        }, 'touchend' : function(){
            $(this).removeClass('touch');
        }
    });
}

function popupOpen(target){
    $('.popup').stop().fadeOut(200);
    $(target).stop().fadeIn(200);
}

function popupClose(target){
    $(target).stop().fadeOut(200);
}

function layerOpen(target){
    $('.layer').removeClass('on');
    $(target).addClass('on');
}

function layerClose(target){
    $(target).removeClass('on');
}

function layerTimer(target){
    $(target).addClass('on');
    layerTime = setTimeout(function(){
        $(target).removeClass('on');
        clearTimeout(layerTime);
    }, 2000);
}

function starPoint(){
    $('.star').each(function(){
       var num = $(this).closest('.point').find('.num span').text();
       $(this).find('span').css('width', (num*10*2) + '%');
    });
}

function amountEvent(){
    $('.amount-select').each(function(){
        var amt = $(this);
        amt.find('.btn-minus').on('click', function(){
            var num = parseInt(amt.find('span').text());
           if(num > 1){
               if(num >2){
                   amt.find('span').text(num-1);
               } else {
                   $(this).addClass('disabled');
                   amt.find('span').text(num-1);
               }
           }
        });

        amt.find('.btn-plus').on('click', function(){
            var num = parseInt(amt.find('span').text());
            $(this).removeClass('disabled');
            amt.find('span').text(num+1);
        });
    });
}

function calendarVertical(){
    $('.calendar-vertical').each(function(){
       var cvc = $(this);
        cvc.find('.day').on('click', function(){
           if(!$(this).hasClass('disabled')){
               cvc.find('.day.on').removeClass('on');
               $(this).addClass('on');
           }
       });
    });
}

function timeSelect(){
    if($('.timer').length > 0){
        $('.timer').drum({
            maxSpinOffset: 100
        });
    }
}

function cardEdit(){
    $('.card-insert').find('.btn-change').on('click', function(){
       var wrp = $(this).closest('.card-insert');
       wrp.addClass('on');
    });
}

function cartItemOption(){
    $('.cart .btn-option').on('click', function(){
        if(!$(this).hasClass('on')){
            $(this).addClass('on');
            $(this).closest('li').find('.option-cont').show();
        } else {
            $(this).removeClass('on');
            $(this).closest('li').find('.option-cont').hide();
        }
    });
}

function maxLengthCheck(object){
    if (object.value.length > object.maxLength){
        object.value = object.value.slice(0, object.maxLength);
    }
}

function containerScroll(){
    $('#container').on('scroll', function(){
        var winST = $(this).scrollTop();
        if(winST > 48){
            $('#header').addClass('scrolled');
        } else {
            $('#header').removeClass('scrolled');
        }
    });
}

function heartToggle(){
    $('.btn-heart').on('click', function(){
       if(!$(this).hasClass('on')){
           $(this).addClass('on');
       } else {
           $(this).removeClass('on');
       }
    });
}

function inputInfoTooltip(){
    $('.input-under .information').on('click', function(){
        if(!$(this).hasClass('on')){
            $(this).addClass('on');
        } else {
            $(this).removeClass('on');
        }
    });
}

function editInline(){
    $('.edit-inline').find('.btn-edit').on('click', function(){
        if(!$(this).closest('.edit-inline').hasClass('on')){
            $(this).closest('.edit-inline').addClass('on');
            $(this).closest('.edit-inline').find('dl').hide();
            $(this).closest('.edit-inline').find('.input-under').show();
            $(this).closest('.edit-inline').find('.input-under input').focus();
        } else {
            var val = $(this).closest('.edit-inline').find('.input-under input').val();
            $(this).closest('.edit-inline').removeClass('on');
            $(this).closest('.edit-inline').find('dl').show();
            $(this).closest('.edit-inline').find('.input-under').hide();
            if(val != ''){
                $(this).closest('.edit-inline').find('dl dd').text(val);
            } else {
                $(this).closest('.edit-inline').find('dl dd').text('None');
            }
        }

    });
}

function tabEvent(){
    $('.tab-js').find('a').on('click', function(){
        var Idx = $(this).index();
        $(this).closest('.tab-js').find('a').removeClass('on');
        $(this).addClass('on');
        $('.tab-content').removeClass('on').eq(Idx).addClass('on');
    });
}

function sliderEvent(){
    if($('#container').hasClass('main')){
        var mainTopSwiper = new Swiper('.top-slider .swiper-container', {
            loop: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
                clickable: true
            }
        });

        var sliderGoodsSwiper = new Swiper('.slider-goods .swiper-container', {
            loop: true,
            spaceBetween: 16,
            pagination: {
                el: '.swiper-pagination'
            }
        });

        $('.top-slider').find('.swiper-pagination').on('click', function(){
            mainTopSwiper.slideNext();
        })
    }
}

function sortToggleEvent(){
    $('#header .btn-sort').on('click', function(){
        if(!$('.sort-area').hasClass('on')){
            $('.sort-area').addClass('on');
        } else {
            $('.sort-area').removeClass('on');
        }
    });

    $('.sort-area').find('.btn-sort-close, .sort-overlay').on('click', function(){
        $('.sort-area').removeClass('on');
    });
}

function quToggle(){
    $('.qu-wrap').find('.btn-qu').on('click', function(){
        if(!$(this).hasClass('on')){
            $(this).addClass('on');
            $(this).closest('.qu-wrap').find('.answer').addClass('on');
        } else {
            $(this).removeClass('on');
            $(this).closest('.qu-wrap').find('.answer').removeClass('on');
        }
    });
}

function accordionEvent(){
    $('.accordion').find('.accordion-toggle').on('click', function(){
       if(!$(this).hasClass('on')){
           $(this).addClass('on');
           $(this).closest('.accordion').find('.accordion-content').addClass('on');
       } else {
           $(this).removeClass('on');
           $(this).closest('.accordion').find('.accordion-content').removeClass('on');
       }
    });

    $('.accordion-content').find('input').on('change', function(){
        if($(this).prop('checked') != false){
            $(this).closest('li').addClass('on');
        } else {
            $(this).closest('li').removeClass('on');
        }
    });
}

function reviewPicPop(){
    $('.store-detail .picture-area').find('img').on('click', function(){
        var src = $(this).attr('src');
        $('.pop-picture').find('img').attr('src', src);
        popupOpen('.pop-picture');
    });

    $('.pop-picture').find('.btn-close').on('click', function(){
        popupClose('.pop-picture');
    });
}